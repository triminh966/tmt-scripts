const https = require('https');

async function sendRequest() {
  const req = https.request(options, (res) => {
    console.log('statusCode:', res.statusCode);
    console.log('headers:', res.headers);
  
    res.on('data', (data) => {
      return data;
    });
  });

  req.on('error', (e) => {
    console.error(e);
  });
  
  req.write(JSON.stringify(data));
  req.end();
};

const options = {
  hostname: 'https://api.sit.orangetheoryfitness.net',
  port: 443,
  path: 'crm-node/v1/leads',
  method: 'POST',
  headers: {
    'Content-Type': 'application/json'
  }
}

const data = { "desiredHomeStudio": 2129, "firstName": "Trm", "lastName": "Minh", 
"postalCode": null, "territory": null, "territoryAbbr": null, "country": "US", "email": "testdemo002@gmail.com", 
"address1": null, "city": null, "preferredPhone": null, "homePhone": null, "mobilePhone": null, "workPhone": null, 
"contactPreference": null, "collectionSource": "Walk-In Studio", "collectionSourceEvent": null, "collectionSourceReferrer": null, 
"leadStatus": "Lead", "leadTemperature": "Hot", "leadSalesAssociate": "a3b4beb0-9c82-4764-a6bd-bde98f097290", 
"termConditions": { "sysEmail": true, "sysText": false }, "history": { "profileHistory": { "type": "Add Lead" } } }

sendRequest();